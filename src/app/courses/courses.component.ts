import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  blue=require('../img/blue.jpg');
  orange=require('../img/orange.jpg');
  red=require('../img/red.jpg');
  green=require('../img/green.jpg');
}
