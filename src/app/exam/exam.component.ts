import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FirebaseService } from '../services/firebaseService';
import { Question, AnswerResponse } from '../models/QuestionModel';
import { NgxLoadingSpinnerService } from 'ng-loading-spinner';
import { CountdownComponent } from 'ngx-countdown';
import {NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-exam',
  templateUrl: './exam.component.html',
  styleUrls: ['./exam.component.css']
})
export class ExamComponent implements OnInit {
  //@ViewChild(CountdownComponent) counter: CountdownComponent;
 
  public QuestionList: Array<Question>;
  public AnswerList: AnswerResponse[] = [];
  public DataFlag : boolean = false
  public config = {
    leftTime: 30
  }

  public  CorrectAnswerCount : number = 0;
  public WrongAnswerCount : number = 0;
  constructor(
    public dataservice: FirebaseService,
    private spinner: NgxLoadingSpinnerService,
    private modalService: NgbModal,
     config:NgbModalConfig
  ) { 
  config.backdrop  = 'static';
  config.keyboard = false;
  }

  ngOnInit() {
    this.spinner.start();
    console.log("started");
    this.GetQuestions();
    
    // this.videoLibraryAlertPopup
  }
  public GetQuestions() {
    var fireRequest = this.dataservice.getAllQuestions().subscribe((fireResponse) => {

      this.QuestionList = fireResponse.map(e => {
        return {
          A: e.payload.doc.data()['A'],
          B: e.payload.doc.data()['B'],
          C: e.payload.doc.data()['C'],
          D: e.payload.doc.data()['D'],
          Question: e.payload.doc.data()['QUESTIONS'],
          Correct: e.payload.doc.data()['Correct'],
          Id: e.payload.doc.data()['SN']
        }
      });
      this.spinner.stop();
      this.DataFlag = true;
      console.log(this.QuestionList);
    });
  }
   
  public StartTest() {
    this.modalService.dismissAll();
  }

  public GetSelectedAnswer(selectedValue, correct, questionId, question) {
    debugger;
    var objanswerResponse: AnswerResponse = new AnswerResponse(selectedValue, correct, questionId, question);

    const index = this.AnswerList.findIndex((res) => res.QuestionId === objanswerResponse.QuestionId)
    if (index === -1) {
      this.AnswerList.push(objanswerResponse)
    }
    else {
      this.AnswerList[index] = objanswerResponse
    }
    console.log(this.AnswerList);
  }

  public SubmitTest() {
    for(let i = 0 ; i< this.AnswerList.length; i ++) {
        if(this.AnswerList[i].Correct == this.AnswerList[i].Selected) {
          this.CorrectAnswerCount +=1;
        }
        else {
          this.WrongAnswerCount +=1 ;
        }
    }
  }
}
