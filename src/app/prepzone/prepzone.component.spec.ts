import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrepzoneComponent } from './prepzone.component';

describe('PrepzoneComponent', () => {
  let component: PrepzoneComponent;
  let fixture: ComponentFixture<PrepzoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrepzoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrepzoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
