export class Question {
    A : string ;
    B : string ;
    C : string;
    D: string ;
    Question : string;
    Id : number ;
    Correct : string;
    constructor(response : Question) 
    {
        this.A  = response.A;
        this.B  = response.B;
        this.C = response.C;
        this.D = response.D;
        this.Question = response.Question;
        this.Correct = response.Correct;
    }
}

export class AnswerResponse {
    Selected : string ;
    Correct : string ;
    QuestionId: string;
    Question: string;
    constructor(selected, correct,questionId,question) {
        this.Selected =selected;
        this.Correct = correct;
        this.QuestionId = questionId;
        this.Question  = question;
    }
}