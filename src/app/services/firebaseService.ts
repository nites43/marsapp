import { Injectable } from '@angular/core';
 
import { AngularFirestore } from '@angular/fire/firestore';
 
@Injectable({
  providedIn: 'root'
})
export class FirebaseService 
{
    constructor(
        private firestore: AngularFirestore
    ) {}
    public getAllQuestions() {
        return this.firestore.collection('Questions').snapshotChanges();
    }
}