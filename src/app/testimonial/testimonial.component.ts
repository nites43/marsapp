import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-testimonial',
  templateUrl: './testimonial.component.html',
  styleUrls: ['./testimonial.component.css']
})
export class TestimonialComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  boy=require('../img/boy.jpg');
  archana=require('../img/testimonial/archana.jpg');
  ashok=require('../img/testimonial/ashok.jpg');
  pallavi=require("../img/testimonial/pallavi.jpg");
  shilpi=require('../img/testimonial/shilpi.jpg');
  sudeep=require('../img/testimonial/sudeep.jpg');
  tajalli=require('../img/testimonial/tajalli.jpg')
}
