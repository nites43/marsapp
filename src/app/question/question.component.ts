import { Component, OnInit } from '@angular/core';
// import { Http } from '@angular/http';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {
  // posts: any[];
  // private url='https://marsproject-ba33b.firebaseio.com/marsproject-ba33b';
  // constructor(private http: Http) { 
  //   http.get(this.url)
  //     .subscribe(response=>{
  //       this.posts=response.json();
  //       console.log(response.json);
  //     });
  // }
  // items: Observable<any[]>;
  // constructor(db: AngularFirestore) {
  //   this.items = db.collection('').valueChanges();
  // }
  constructor( db: AngularFirestore) {
    const things = db.collection('things').valueChanges();
    things.subscribe(console.log);
}
  ngOnInit() {
  }

}
